//
//  Forecast.swift
//  mango
//
//  Created by Dhawan, Jatin on 10/16/18.
//  Copyright © 2018 Dhawan, Jatin. All rights reserved.
//

import Foundation

struct Forecast {
    let locationName : String
    let country : String
    let temperature : Double
    let humidity : Double
    let rainChances : String
    let wind : Double
}

struct DailyForecast {
    let todaysDate : Date
    let temperature : Double
    let humidity : Double
    let rainChances : String
    let wind : Double
}
