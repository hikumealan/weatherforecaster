//
//  DataManager.swift
//  mango
//
//  Created by Dhawan, Jatin on 10/16/18.
//  Copyright © 2018 Dhawan, Jatin. All rights reserved.
//

import Foundation

struct key {
    static let firstLaunch = "firstLaunch"
    static let locations = "locations"
    static let unit = "unit"
}

let defaultUnit = Unit.imperial

class CacheInteractor {
    
    static var isFirstLaunch: Bool {
        guard let _ = UserDefaults.standard.object(forKey: key.firstLaunch) as? Bool else {
            UserDefaults.standard.set(false, forKey: key.firstLaunch)
            return true
        }

        return false
    }

    static var bookmarkedLocations: [Location] {
        guard let decode = UserDefaults.standard.object(forKey: key.locations) as? Data else {
            return []
        }
        guard let locations = NSKeyedUnarchiver.unarchiveObject(with: decode) as? [Location] else {
            return []
        }
        return locations
    }

    static var unit: Unit {
        if let value =  UserDefaults.standard.object(forKey: key.unit) as? String {
            return Unit(rawValue: value)!
        }
        return defaultUnit
    }

    static func updateLocations(_ locations: [Location]) {
        UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: locations), forKey: key.locations)
    }

    
    static func updateSettings(_ unit: Unit) {
        UserDefaults.standard.set(unit.rawValue, forKey: key.unit)
    }
}

