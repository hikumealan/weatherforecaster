//
//  Settings.swift
//  mango
//
//  Created by Dhawan, Jatin on 10/16/18.
//  Copyright © 2018 Dhawan, Jatin. All rights reserved.
//

import Foundation

enum Unit: String {
    case imperial = "imperial"
    case metric = "metric"

    var windSpeedString: String {
        switch self {
        case .imperial:
            return " mph"
        case .metric:
            return " mps"
        }
    }

    var temperatureString: String {
        switch self {
        case .imperial:
            return " °F"
        case .metric:
            return " °C"
        }
    }
}
