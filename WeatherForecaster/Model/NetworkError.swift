//
//  ApplicationError.swift
//  WeatherForecaster
//
//  Created by Dhawan, Jatin on 2018-10-18.
//  Copyright © 2018 CAPCO. All rights reserved.
//

import Foundation

enum NetworkError: Error {
    case parsingError
    case noContent
    case unknownError
    case serviceError

    var description: String {
        switch self {
        case .parsingError:
            return "An issue has been found with data"
        case .noContent:
            return "No Content found"
        case .unknownError:
            return "Unknown error occured"
        case .serviceError:
            return self.localizedDescription
        }
    }
}
