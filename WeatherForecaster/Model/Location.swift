//
//  Location.swift
//  mango
//
//  Created by Dhawan, Jatin on 10/16/18.
//  Copyright © 2018 Dhawan, Jatin. All rights reserved.
//

import Foundation
import CoreLocation

class Location: NSObject, NSCoding {
    let name : String
    let lat : Double
    let lon : Double
    
    init(name : String, lat: Double, lon: Double) {
        self.name = name
        self.lat = lat
        self.lon = lon
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let name = aDecoder.decodeObject(forKey: "name") as! String
        let lat = aDecoder.decodeDouble(forKey: "lat")
        let lon = aDecoder.decodeDouble(forKey: "lon")
        self.init(name : name, lat: lat, lon: lon)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "name")
        aCoder.encode(lat, forKey: "lat")
        aCoder.encode(lon, forKey: "lon")
    }

    var coordinate2D: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: lat, longitude: lon)
    }
}
