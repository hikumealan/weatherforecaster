//
//  HelpViewController.swift
//  WeatherForecaster
//
//  Created by Dhawan, Jatin on 2018-10-17.
//  Copyright © 2018 CAPCO. All rights reserved.
//

import UIKit
import WebKit

class HelpViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true

        let configuration = WKWebViewConfiguration()
        configuration.preferences = preferences

        let webView = WKWebView(frame: self.view.bounds, configuration: configuration)
        self.view.addSubview(webView)

        let url = Bundle.main.url(forResource: "index", withExtension: "html")!
        let request = URLRequest(url: url)
        webView.load(request)
    }
}
