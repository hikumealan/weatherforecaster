//
//  DetailViewController.swift
//  WeatherForecaster
//
//  Created by Dhawan, Jatin on 2018-10-16.
//  Copyright © 2018 CAPCO. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var windyLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var rainLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var emptyLabel: UILabel!
    @IBOutlet weak var containerTableView: UITableView!

    var weeklyForecast: [Date : [DailyForecast]] = [:]
    var alert: UIAlertController = UIAlertController(title: "Error", message: "", preferredStyle: .alert )

    var location: Location? {
        didSet {
            self.configureView()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        containerTableView.register(UINib(nibName: "HeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "HeaderView")

        self.alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        // Added in ViewDidAppear to refresh view to handle use cases like when returning from Settings etc.
        self.configureView()
    }

    fileprivate func configureView() {
        if let location = self.location {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true

            //TODO: Combine the below 2 API calls in a Serial Queue using GCD or OperationQueue
            API.shared.getCurrentWeather(location.coordinate2D) { forecast, error in
                if let error = error as? NetworkError {
                    self.alert.message = error.description
                    if !self.alert.isBeingPresented { self.present(self.alert, animated: true, completion: nil) }
                    return
                }

                if let forecast = forecast {
                    self.emptyLabel.isHidden = true
                    self.locationLabel.text = "\(forecast.locationName), \(forecast.country)"
                    self.windyLabel.text = "Wind Speed: \(forecast.wind)" + CacheInteractor.unit.windSpeedString
                    self.temperatureLabel.text = "Temp: \(forecast.temperature)" + CacheInteractor.unit.temperatureString
                    self.rainLabel.text = "Rain Chances: \(forecast.rainChances)"
                    self.humidityLabel.text = "Humidity: \(forecast.temperature)%"
                }

                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }

            API.shared.getForecast(location.coordinate2D) { (forecast, error) in
                if let error = error as? NetworkError {
                    self.alert.message = error.description
                    if !self.alert.isBeingPresented { self.present(self.alert, animated: true, completion: nil) }
                    return
                }
                self.containerTableView.isHidden = false
                self.weeklyForecast = Dictionary(grouping: forecast, by: { $0.todaysDate.formattedDate ?? Date() })
                self.containerTableView.reloadData()
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        } else {
            self.emptyLabel.isHidden = false
        }
    }
}

extension DetailViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return weeklyForecast.keys.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! DayTableViewCell
        cell.collectionView.reloadData()

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }
}

extension DetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderView") as! HeaderView
        headerView.customLabel.text = Array(weeklyForecast.keys.sorted(by: { $0 < $1 }))[section].dayOfWeek
        return headerView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
}

extension DetailViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let key = Array(weeklyForecast.keys.sorted(by: { $0 < $1 }))[section]
        return weeklyForecast[key]?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "rainyCollectionViewCell",
                                                        for: indexPath) as! RainyCollectionViewCell
        let key = Array(weeklyForecast.keys.sorted(by: { $0 < $1 }))[indexPath.section]
        cell.updateView(forecast: weeklyForecast[key]?[indexPath.row])
        return cell
    }
}

extension DetailViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //TODO: Need to implement better dynamic width because the API returns 'n' number of remaining 3HR forecast based on the time of the day
        return CGSize(width: 100, height: 100)
    }
}

