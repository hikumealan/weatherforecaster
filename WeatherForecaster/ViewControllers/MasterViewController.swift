//
//  MasterViewController.swift
//  WeatherForecaster
//
//  Created by Dhawan, Jatin on 2018-10-16.
//  Copyright © 2018 CAPCO. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {

    let searchController = UISearchController(searchResultsController: nil)
    var detailViewController: DetailViewController? = nil

    fileprivate var locations: [Location] = CacheInteractor.bookmarkedLocations
    fileprivate var searchString: String?
    fileprivate var filteredLocations = [Location]()

    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.leftBarButtonItem = self.editButtonItem

        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addBookmark(_:)))
        self.navigationItem.rightBarButtonItem = addButton
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }

        searchController.searchResultsUpdater = self
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Locations"
        self.tableView.tableHeaderView = searchController.searchBar
    }

    override func viewWillAppear(_ animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        locations = CacheInteractor.bookmarkedLocations
        self.tableView.reloadData()
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.location = isFiltering ? filteredLocations[indexPath.row] : locations[indexPath.row]
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
            searchController.isActive = false
        }
    }


    // MARK: - TableViewDataSource

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltering ? filteredLocations.count : locations.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let location = isFiltering ? filteredLocations[indexPath.row] : locations[indexPath.row]

        cell.textLabel!.text = location.name
        return cell
    }

    //MARK: UITableViewDelegate

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            locations.remove(at: indexPath.row)
            CacheInteractor.updateLocations(locations)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }

    // MARK: - Private instance methods

    fileprivate var isFiltering: Bool {
        return searchController.isActive && !searchBarIsEmpty
    }

    fileprivate var searchBarIsEmpty: Bool {
        // Returns true if the text is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }

    fileprivate func filterContentForSearchText(_ searchText: String) {
        filteredLocations = locations.filter({ $0.name.lowercased().contains(searchText.lowercased()) })
        tableView.reloadData()
    }

    @objc func addBookmark(_ sender: AnyObject) {
        performSegue(withIdentifier: "mapsSegue", sender: nil)
    }
}

extension MasterViewController: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
}
