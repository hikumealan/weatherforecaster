//
//  SettingsViewController.swift
//  mango
//
//  Created by Dhawan, Jatin on 10/17/18.
//  Copyright © 2018 Dhawan, Jatin. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var unitsSegmentControl: UISegmentedControl!

    @IBAction func resetAllBookmarks() {
        let alert = UIAlertController(title: "Are you sure?", message: "You would like to remove all bookmarks.", preferredStyle: .alert )

        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
            CacheInteractor.updateLocations([])
        }))

        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))

        self.present(alert, animated: true)
    }

    @IBAction func segmentControlDidChange(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            CacheInteractor.updateSettings(Unit.imperial)
        } else if sender.selectedSegmentIndex == 1 {
            CacheInteractor.updateSettings(Unit.metric)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        switch CacheInteractor.unit {
        case .imperial:
            unitsSegmentControl.selectedSegmentIndex = 0
        case .metric:
            unitsSegmentControl.selectedSegmentIndex = 1
        }
    }
}
