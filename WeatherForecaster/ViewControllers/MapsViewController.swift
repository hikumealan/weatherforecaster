//
//  MapsViewController.swift
//  mango
//
//  Created by Dhawan, Jatin on 10/17/18.
//  Copyright © 2018 Dhawan, Jatin. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

struct const {
    static let precision = 0.01
}

class MapsViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate, UIGestureRecognizerDelegate {
    
    var locationManager: CLLocationManager!
    var annotation: MKPointAnnotation = MKPointAnnotation()
    var forecast: Forecast?

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var saveButton: UIBarButtonItem!

    @IBAction func dismissMapsView(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func saveBookmark(_ sender: UIBarButtonItem) {
        guard let forecast = forecast else {
            print("No Bookmarked Location Found")
            return
        }

        var locations = CacheInteractor.bookmarkedLocations

        guard locations.filter({ $0.name.lowercased() ==  forecast.locationName.lowercased()}).count == 0  else {
            self.presentError(message: "Location Already Bookmarked!")
            return
        }
        
        locations.append(Location(name: forecast.locationName, lat: self.annotation.coordinate.latitude, lon: self.annotation.coordinate.longitude))
        CacheInteractor.updateLocations(locations)
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        mapView.addGestureRecognizer(tapGesture)

        let doubleTap = UITapGestureRecognizer(target: self, action: nil)
        doubleTap.numberOfTapsRequired = 2
        mapView.addGestureRecognizer(doubleTap)

        tapGesture.require(toFail: doubleTap)

        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingHeading()
            locationManager.startUpdatingLocation()
        }

        mapView.addAnnotation(annotation)
    }
    
    @objc func handleTap(_ gestureRecognizer: UITapGestureRecognizer) {
        self.saveButton.isEnabled = false //Handles use case if user tries to save location on a slow internet connection

        let touchPoint = gestureRecognizer.location(in: mapView)
        let location = mapView.convert(touchPoint, toCoordinateFrom: mapView)
        annotation.coordinate = location

        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        API.shared.getCurrentWeather(annotation.coordinate) { forecast, error in
            if let error = error as? NetworkError {
                self.presentError(message: error.description)
            }

            if let forecast = forecast {
                self.annotation.title = forecast.locationName
                self.saveButton.isEnabled = true
                self.forecast = forecast
            }

            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let currentLocation:CLLocation = locations[0] as CLLocation

        manager.stopUpdatingLocation()

        let coord = CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
        let region = MKCoordinateRegion(center: coord, span: MKCoordinateSpan(latitudeDelta: const.precision, longitudeDelta: const.precision))

        mapView.setRegion(region, animated: true)
    }
}
