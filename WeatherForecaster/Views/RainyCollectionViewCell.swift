//
//  WeatherCollectionViewCell.swift
//  WeatherForecaster
//
//  Created by Dhawan, Jatin on 2018-10-19.
//  Copyright © 2018 CAPCO. All rights reserved.
//

import UIKit

class RainyCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var rainLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!

    func updateView(forecast: DailyForecast?) {
        guard let forecast = forecast else { return }
        self.dayLabel.text = forecast.todaysDate.time
        self.windLabel.text = "\(forecast.wind)" + CacheInteractor.unit.windSpeedString
        self.tempLabel.text = "\(forecast.temperature)" + CacheInteractor.unit.temperatureString
        self.rainLabel.text = "\(forecast.rainChances)"
        self.humidityLabel.text = "\(forecast.humidity)%"
    }
}
