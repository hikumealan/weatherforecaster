//
//  HeaderView.swift
//  WeatherForecaster
//
//  Created by Dhawan, Jatin on 2018-10-19.
//  Copyright © 2018 CAPCO. All rights reserved.
//

import UIKit

class HeaderView: UITableViewHeaderFooterView {
    @IBOutlet weak var customLabel: UILabel!

    override func awakeFromNib() {
        let view = UIView(frame: self.bounds)
        view.backgroundColor = UIColor.lightGray
        self.backgroundView = view
    }
}
