//
//  DayTableViewCell.swift
//  WeatherForecaster
//
//  Created by Dhawan, Jatin on 2018-10-21.
//  Copyright © 2018 CAPCO. All rights reserved.
//

import UIKit

class DayTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
