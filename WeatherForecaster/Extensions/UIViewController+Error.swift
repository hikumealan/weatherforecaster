//
//  UIViewController+Error.swift
//  WeatherForecaster
//
//  Created by Dhawan, Jatin on 2018-10-18.
//  Copyright © 2018 CAPCO. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func presentError(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert )

        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))

        self.present(alert, animated: true)
    }
}
