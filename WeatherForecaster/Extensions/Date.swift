//
//  Date+String.swift
//  WeatherForecaster
//
//  Created by Dhawan, Jatin on 2018-10-19.
//  Copyright © 2018 CAPCO. All rights reserved.
//

import Foundation

extension Date {
    var dayOfWeek: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self).capitalized
    }

    var formattedDate: Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let dateString =  dateFormatter.string(from: self)

        return dateFormatter.date(from: dateString)
    }

    var time: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h a"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        return dateFormatter.string(from: self)
    }
}
