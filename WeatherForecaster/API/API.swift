//
//  API.swift
//  WeatherForecaster
//
//  Created by Dhawan, Jatin on 2018-10-17.
//  Copyright © 2018 CAPCO. All rights reserved.
//

import Foundation
import CoreLocation

enum Route: String {
    case weather
    case forecast
}

final class API {
    // MARK: - Properties

    static let shared = API(baseURL: URL(string: "http://api.openweathermap.org/data/2.5")!)

    // MARK: -

    let baseURL: URL

    // Initialization

    fileprivate init(baseURL: URL) {
        self.baseURL = baseURL
    }

    func getCurrentWeather(_ location: CLLocationCoordinate2D, completionHandler: @escaping (Forecast?, Error?) -> Void) {
        let task = URLSession.shared.dataTask(with: createURLWithComponents(location, .weather)!, completionHandler: {(data, response, error) in

            guard error == nil else {
                DispatchQueue.main.async { completionHandler(nil, error) }
                return
            }

            guard let content = data else {
                DispatchQueue.main.async { completionHandler(nil, NetworkError.noContent) }
                return
            }

            guard let json = (try? JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                DispatchQueue.main.async { completionHandler(nil, NetworkError.parsingError) }
                return
            }

            guard let main = json["main"] as? [String: Any?],
                let weatherArray = json["weather"] as? [Any],
                let weather = weatherArray.first as? [String: Any?],
                let rainChances = weather["main"] as? String,
                let name = json["name"] as? String,
                let sys = json["sys"] as? [String: Any?],
                let country = sys["country"] as? String,
                let wind = json["wind"] as? [String: Any?],
                let temperature = main["temp"] as? Double,
                let humidity = main["humidity"] as? Double,
                let speed = wind["speed"] as? Double else {
                    DispatchQueue.main.async { completionHandler(nil, NetworkError.parsingError) }
                    return
            }

            DispatchQueue.main.async {
                completionHandler(Forecast(locationName: name, country: country, temperature: temperature, humidity: humidity, rainChances: rainChances, wind: speed), nil)
            }
        })
        
        task.resume()
        
    }


    func getForecast(_ location: CLLocationCoordinate2D, completionHandler: @escaping ([DailyForecast], Error?) -> Void) {
        let task = URLSession.shared.dataTask(with: createURLWithComponents(location, .forecast)!, completionHandler: {(data, response, error) in

            guard error == nil else {
                DispatchQueue.main.async { completionHandler([], error) }
                return
            }

            guard let content = data else {
                DispatchQueue.main.async { completionHandler([], NetworkError.noContent) }
                return
            }

            guard let json = (try? JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                DispatchQueue.main.async { completionHandler([], NetworkError.parsingError) }
                return
            }

            guard let list = json["list"] as? Array<[String: Any]>  else {
                DispatchQueue.main.async { completionHandler([], NetworkError.parsingError) }
                return
            }

            DispatchQueue.main.async {
                completionHandler(self.mapitems(jsonItems: list), nil)
            }
        })

        task.resume()

    }

    fileprivate func mapitems(jsonItems: Array<[String: Any]>) -> [DailyForecast] {
        return jsonItems.compactMap { (json: [String: Any]) -> DailyForecast? in
            guard let main = json["main"] as? [String: Any?],
                let weatherArray = json["weather"] as? [Any],
                let weather = weatherArray.first as? [String: Any?],
                let rainChances = weather["main"] as? String,
                let dateInt = json["dt"] as? Int,
                let wind = json["wind"] as? [String: Any?],
                let temperature = main["temp"] as? Double,
                let humidity = main["humidity"] as? Double,
                let speed = wind["speed"] as? Double
                else {
                    return nil
            }

            let timeInterval = Double(dateInt)
            let myNSDate = Date(timeIntervalSince1970: timeInterval)
            return DailyForecast(todaysDate: myNSDate, temperature: temperature, humidity: humidity, rainChances: rainChances, wind: speed)
        }
    }

    fileprivate func createURLWithComponents(_ location: CLLocationCoordinate2D, _ route: Route) -> URL? {
        var urlComponents = URLComponents()
        urlComponents.scheme = "http";
        urlComponents.host = "api.openweathermap.org";
        urlComponents.path = "/data/2.5/\(route.rawValue)";

        let latQuery = URLQueryItem(name: "lat", value: "\(location.latitude)")
        let lonQuery = URLQueryItem(name: "lon", value: "\(location.longitude)")
        let apiKeyQuery = URLQueryItem(name: "appid", value: "c6e381d8c7ff98f0fee43775817cf6ad")
        let unitQuery = URLQueryItem(name: "units", value: CacheInteractor.unit.rawValue)
        urlComponents.queryItems = [latQuery, lonQuery, apiKeyQuery, unitQuery]
        return urlComponents.url
    }
}
