//
//  WeatherForecasterTests.swift
//  WeatherForecasterTests
//
//  Created by Dhawan, Jatin on 2018-10-16.
//  Copyright © 2018 CAPCO. All rights reserved.
//

import XCTest
import CoreLocation
@testable import WeatherForecaster

let timeout: TimeInterval = 20.0

class WeatherForecasterTests: XCTestCase {


    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testWeatherServiceForAKnownLocation() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let exp: XCTestExpectation = self.expectation(description: #function)

        let loc = CLLocationCoordinate2DMake(43.7887231, -79.6789463)
        API.shared.getCurrentWeather(loc) { (forecast, error) in
            if let _ = error {
                XCTFail("Must retrieve Weather object from Weather Service for given Coordinates.")
            }
            exp.fulfill()
        }


        self.waitForExpectations(timeout: timeout) { (error) in
            if let e = error { XCTFail(e.localizedDescription) }
        }
    }

    func testWeatherServiceForAnUnKnownLocation() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let exp: XCTestExpectation = self.expectation(description: #function)

        let loc = CLLocationCoordinate2DMake(25.0, -50.0) //Some point in the sea with no name
        API.shared.getCurrentWeather(loc) { (forecast, error) in
            if let _ = forecast {
                XCTFail("The Service shouldn't return a Location name for these coordinates.")
            }
            exp.fulfill()
        }


        self.waitForExpectations(timeout: timeout) { (error) in
            if let e = error { XCTFail(e.localizedDescription) }
        }
    }

    func testForecastService() {
        let exp: XCTestExpectation = self.expectation(description: #function)

        let loc = CLLocationCoordinate2DMake(43.7887231, -79.6789463)
        API.shared.getForecast(loc) { (forecast, error) in
            if let _ = error {
                XCTFail("Must retrieve Forecast object from Forecast Service for given Coordinates.")
            }

            if forecast.count > 0 {
                exp.fulfill()
            } else if forecast.count == 0 {
                XCTFail("Empty Data Set returned.")
            }
        }


        self.waitForExpectations(timeout: timeout) { (error) in
            if let e = error { XCTFail(e.localizedDescription) }
        }
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
